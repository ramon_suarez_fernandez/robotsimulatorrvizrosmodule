/*
 * Copyright (c) 2011, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Willow Garage, Inc. nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */


#include <ros/ros.h>

#include <interactive_markers/interactive_marker_server.h>
#include <interactive_markers/menu_handler.h>
#include "geometry_msgs/Point.h"
#include "geometry_msgs/Vector3Stamped.h"
#include "droneMsgsROS/robotPose.h"
#include "droneMsgsROS/points3DStamped.h"
#include <visualization_msgs/Marker.h>

#include <tf/transform_broadcaster.h>
#include <tf/tf.h>
#include <algorithm>
#include <sstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
//#include <strstream>

#include <vector>

#include <math.h>

using namespace visualization_msgs;







void Callback_front(const droneMsgsROS::points3DStamped &msg)
{
    std::vector<float> gridpointListFront;
    for(uint32_t i=1; i < msg.points3D.size(); i++){
        gridpointListFront[i] = (msg.points3D[i].x, msg.points3D[i].y);


    }


}

void Callback_back(const droneMsgsROS::robotPose &msg)
{


}
void Callback_left(const droneMsgsROS::robotPose &msg)
{


}
void Callback_right(const droneMsgsROS::robotPose &msg)
{


}
void Callback_bottom(const droneMsgsROS::robotPose &msg)
{


}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "robot_controls");
  ros::NodeHandle n;



  ros::Duration(0.1).sleep();

  ros::Subscriber sub_front = n.subscribe("perception/front/gridIntersections", 1, Callback_front);
//  ros::Subscriber sub_back = n.subscribe("perception/back/gridIntersections", 1, Callback_back);
//  ros::Subscriber sub_left = n.subscribe("perception/left/gridIntersections", 1, Callback_left);
//  ros::Subscriber sub_right = n.subscribe("perception/right/gridIntersections", 1, Callback_right);
//  ros::Subscriber sub_bottom = n.subscribe("perception/bottom/gridIntersections", 1, Callback_bottom);
//  //ros::Publisher marker_pub = n.advertise<visualization_msgs::Marker>("Grid_marker", 10);




  ros::spin();


}
